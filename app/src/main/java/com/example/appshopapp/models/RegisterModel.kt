package com.example.appshopapp.models

data class RegisterModel(
    val OK: Boolean,
    val registered: Boolean
)