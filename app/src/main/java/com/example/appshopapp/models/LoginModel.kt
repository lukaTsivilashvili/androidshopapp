package com.example.appshopapp.models

data class LoginModel(
    val token: String?,
    val user_id: Int?
)