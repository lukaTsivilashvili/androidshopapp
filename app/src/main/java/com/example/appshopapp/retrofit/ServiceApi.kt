package com.example.appshopapp.retrofit

import com.example.appshopapp.models.LoginModel
import com.example.appshopapp.models.RegisterModel
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ServiceApi {

    @POST("login")
    @FormUrlEncoded
    suspend fun logIn(
        @Field("email") email: String,
        @Field("password") password: String
    ): Response<LoginModel>


    @POST("register")
    @FormUrlEncoded
    suspend fun register(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("full_name") fullName: String
    ): Response<RegisterModel>

}