package com.example.appshopapp.retrofit

data class Handler<T>(
    val status: Status,
    val data: T? = null,
    val message: String? = null,
    val loading: Boolean
) {


    enum class Status {
        SUCCESS,
        LOADING,
        ERROR
    }

    companion object{

        fun<T> success(data:T):Handler<T>{
            return Handler(Status.SUCCESS, data, null, false)
        }

        fun<T> error(message:String):Handler<T>{
            return Handler(Status.ERROR, null, message, false)
        }

        fun<T> loading(loading:Boolean):Handler<T>{
            return Handler(Status.LOADING, null, null, loading)
        }

    }


}
