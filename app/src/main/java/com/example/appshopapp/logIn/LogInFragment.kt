package com.example.appshopapp.logIn

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.appshopapp.base.BaseFragment
import com.example.appshopapp.databinding.LogInFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LogInFragment : BaseFragment<LogInFragmentBinding>(LogInFragmentBinding::inflate) {

    private val viewModel:LogInViewModel by viewModels()


    override fun initialize(inflater: LayoutInflater, container: ViewGroup?) {

        binding.btnLogIn.button.setOnClickListener {
            viewModel.logIn(binding.etEmail.text.toString(), binding.etPassword.text.toString())
        }

    }


}