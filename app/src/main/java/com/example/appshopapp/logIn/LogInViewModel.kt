package com.example.appshopapp.logIn

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.appshopapp.repositoy.LogInRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


@HiltViewModel
class LogInViewModel @Inject constructor(private val logInRepo: LogInRepo) : ViewModel() {

    fun logIn(email:String, password:String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val result = logInRepo.logIn(email, password)
            }
        }
    }


}