package com.example.appshopapp.di

import com.example.appshopapp.repositoy.LogInRepo
import com.example.appshopapp.repositoy.LogInRepoImplement
import com.example.appshopapp.retrofit.ServiceApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    companion object {

        private const val BASE_URL = "https://ktorhighsteaks.herokuapp.com/"

    }

    @Provides
    @Singleton
    fun logInService() =
        Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
            .build().create(ServiceApi::class.java)



    @Provides
    @Singleton
    fun addLogInRepo(apiservice:ServiceApi):LogInRepo = LogInRepoImplement(apiservice)

}

