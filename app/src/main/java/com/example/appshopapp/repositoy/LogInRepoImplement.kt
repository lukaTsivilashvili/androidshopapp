package com.example.appshopapp.repositoy

import android.content.Context
import android.util.Log.d
import android.widget.Toast
import com.example.appshopapp.MyAppClass
import com.example.appshopapp.MyAppClass_GeneratedInjector
import com.example.appshopapp.MyAppClass_HiltComponents
import com.example.appshopapp.models.LoginModel
import com.example.appshopapp.retrofit.Handler
import com.example.appshopapp.retrofit.ServiceApi
import dagger.hilt.android.qualifiers.ApplicationContext
import java.lang.Exception
import javax.inject.Inject

class LogInRepoImplement @Inject constructor(

    private val apiServiceApi: ServiceApi
) : LogInRepo {
    override suspend fun logIn(email: String, password: String): Handler<LoginModel> {

        return try {

            val response = apiServiceApi.logIn(email, password)
            if (response.isSuccessful) {
                d("myLog", response.body().toString())
                Handler.success(response.body()!!)
            } else {
                Handler.error(response.errorBody()!!.string())
            }

        } catch (e: Exception) {
            Handler.error(e.message.toString())
        }

    }
}