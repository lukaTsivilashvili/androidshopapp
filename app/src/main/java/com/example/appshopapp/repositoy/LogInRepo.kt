package com.example.appshopapp.repositoy

import com.example.appshopapp.models.LoginModel
import com.example.appshopapp.retrofit.Handler

interface LogInRepo {

    suspend fun logIn(email:String, password:String): Handler<LoginModel>

}